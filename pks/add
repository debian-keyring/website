#!/usr/bin/perl -wT

#
# Simple HKP add script for debian keys.  Accepts queries of the form:
#
#     POST /pks/add HTTP/1.0
#     keytext=<key>
#
# The new keys are stored in an incoming directory to be processed
# periodically.
#
# Copyright (C) 2000 Brendan O'Dea <bod@debian.org>
# Copying policy: GPL (https://www.gnu.org/copyleft/gpl.html)
#

use strict;
use CGI;
use CGI::Carp;
use IO::File;
use Fcntl qw(LOCK_EX);

my $conf     = do '/srv/keyring.debian.org/keyserver.conf';
my $query    = CGI->new;
my $key      = $query->param('keytext');
my $title    = 'Public Key Server -- Error';
my $status   = '500 Server error';
my $content  = $query->p('Server error.');
my $tmp;

END { unlink $tmp if defined $tmp }

TRY: {
    unless (defined $key and $key =~ /PUBLIC KEY BLOCK/)
    {
	$status = '406 "keytext" data required';
	$content = $query->p('Invalid query.  "keytext" data required.');
	last TRY;
    }

    chdir $conf->{INCOMING} or do {
	carp "can't chdir to $conf->{INCOMING} ($!)\n";
	last TRY;
    };

    my $control = IO::File->new($conf->{INCOMING}.'/'.$conf->{CONTROL}, "a+")
    		or do {
	carp "can't open $conf->{CONTROL} ($!)\n";
	last TRY;
    };

    my $id = '0' x 8;

    # grab the next sequence number
    unless (flock $control, LOCK_EX
	and $control->seek(0, SEEK_SET))
    {
	carp "can't lock $conf->{CONTROL} ($!)\n";
	last TRY;
    }

    $id = $1 if defined ($_ = <$control>) and /^(\d{8})/;

    # update the file
    unless ($control->truncate(0) and $control->seek(0, SEEK_SET)
	and $control->print((substr +(sprintf "%08d", $id + 1), -8), "\n"))
    {
	carp "can't update $conf->{CONTROL} ($!)\n";
	last TRY;
    }

    # release lock
    $control->close;

    # dump input to the file
    $tmp = $conf->{'INCOMING'}."/$id.tmp";
    my $queue = IO::File->new(">$tmp") or do {
	carp "can't create $tmp ($!)\n";
	last TRY;
    };

    $queue->print($key) or do {
	carp "can't write $tmp ($!)\n";
	last TRY;
    };

    $queue->close;

    if (rename $tmp, $conf->{INCOMING}.'/'.$id)
    {
	$status = '200 Key data accepted';
	$title = 'Public Key Server -- Accepted';
	$content = $query->p('Key accepted for processing.');
    }
    else
    {
	carp "can't rename $tmp to $id ($!)\n";
    }
}

print $query->header(-status => $status),
      $query->start_html($title),
      $query->h1($title),
      $content,
      $query->end_html;

exit;
